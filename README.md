# shrinkwrapped-project #

Has its dependencies shrink-wrapped.

### What is this repository for? ###

* A trivial npm module for testing purposes

### Install as a Dependency ###

```sh
npm install --save shrinkwrapped-project
```
