const nonEmptyProj = require('not-so-empty-project')

module.exports = function(data) {
  console.log(`shrinkwrapped-project says: I will invoke ${data.toString()}.`)
  return nonEmptyProj(data)
}
